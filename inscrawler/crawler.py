from __future__ import unicode_literals
from builtins import open
from selenium.webdriver.common.keys import Keys

from .exceptions import RetryException
from .browser import Browser
from .utils import instagram_int
from .utils import retry
from .utils import randmized_sleep
from . import secret
import json
import time
from time import sleep
from tqdm import tqdm
import os
import glob
from datetime import datetime

class Logging(object):
    PREFIX = 'instagram-crawler'

    def __init__(self):
        try:
            timestamp  = int(time.time())
            self.cleanup(timestamp)
            self.logger = open('/tmp/%s-%s.log' % (Logging.PREFIX, timestamp), 'w')
            self.log_disable = False
        except:
            self.log_disable = True

    def cleanup(self, timestamp):
        days = 86400 * 7
        days_ago_log = '/tmp/%s-%s.log' % (Logging.PREFIX, timestamp - days)
        for log in glob.glob("/tmp/instagram-crawler-*.log"):
            if log < days_ago_log:
                os.remove(log)

    def log(self, msg):
        if self.log_disable: return

        self.logger.write(msg + '\n')
        self.logger.flush()

    def __del__(self):
        if self.log_disable: return
        self.logger.close()


class InsCrawler(Logging):
    URL = 'https://www.instagram.com'
    RETRY_LIMIT = 10

    def __init__(self, has_screen=False):
        super(InsCrawler, self).__init__()
        self.browser = Browser(has_screen)
        self.page_height = 0

    def _dismiss_login_prompt(self):
        ele_login = self.browser.find_one('.Ls00D .Szr5J')
        if ele_login:
            ele_login.click()

    def login(self):
        browser = self.browser
        url = '%s/accounts/login/' % (InsCrawler.URL)
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(secret.username)
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys(secret.password)

        login_btn = browser.find_one('.L3NKy')
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()

    def get_user_profile(self, username):
        browser = self.browser
        url = '%s/%s/' % (InsCrawler.URL, username)
        browser.get(url)
        name = browser.find_one('.rhpdm')
        desc = browser.find_one('.-vDIg span')
        photo = browser.find_one('._6q-tv')
        statistics = [ele.text for ele in browser.find('.g47SY')]
        post_num, follower_num, following_num = statistics
        return {
            'name': name.text,
            'desc': desc.text if desc else None,
            'photo_url': photo.get_attribute('src'),
            'post_num': post_num,
            'follower_num': follower_num,
            'following_num': following_num
        }

    def get_user_posts(self, username, number=None, detail=False, login=False, dateuntil=19000101):
        browser = self.browser
        
        if login:
            self.login()
            # Check "Turn On Notification"
            sleep(0.2)
            not_now_btn = browser.find_one(".piCib .mt3GC .aOOlW.HoLwm")
            if not_now_btn:
                not_now_btn.click()

        user_profile = self.get_user_profile(username)
        if not number:
            number = instagram_int(user_profile['post_num'])

        self._dismiss_login_prompt()

        if detail:
            return self._get_posts_full(number, login, datetime.strptime(dateuntil, '%Y%m%d'))
        else:
            return self._get_posts(number, login, datetime.strptime(dateuntil, '%Y%m%d'))

    def get_latest_posts_by_tag(self, tag, num):
        url = '%s/explore/tags/%s/' % (InsCrawler.URL, tag)
        self.browser.get(url)
        return self._get_posts(num)

    def auto_like(self, tag='', maximum=1000):
        self.login()
        browser = self.browser
        if tag:
            url = '%s/explore/tags/%s/' % (InsCrawler.URL, tag)
        else:
            url = '%s/explore/' % (InsCrawler.URL)
        self.browser.get(url)

        ele_post = browser.find_one('.v1Nh3 a')
        ele_post.click()

        for _ in range(maximum):
            heart = browser.find_one('.coreSpriteHeartOpen')
            if heart:
                heart.click()
                randmized_sleep(2)

            left_arrow = browser.find_one('.HBoOv')
            if left_arrow:
                left_arrow.click()
                randmized_sleep(2)
            else:
                break

    def _get_posts_full(self, num, login, dateuntil, hashtag=0):
        @retry()
        def check_next_post(cur_key):
            ele_a_datetime = browser.find_one('.eo2As .c-Yi7')
            next_key = ele_a_datetime.get_attribute('href')
            if cur_key == next_key:
                raise RetryException()

        browser = self.browser
        browser.implicitly_wait(1)
        ele_post = browser.find_one('.v1Nh3 a')
        ele_post.click()

        dict_posts = {}
        comment_num = 0
        like_num = 0
        view_num = 0

        pbar = tqdm(total=num)
        pbar.set_description('fetching')
        cur_key = None

        if hashtag == 1:
            compare_date = False
        else:
            compare_date = True

        # Fetching all posts
        for rnum in range(num):
            check_next_post(cur_key)
            dict_post = {}
            dict_post['author']= ''
            # Fetching datetime and url as key
            ele_a_datetime = browser.find_one('.eo2As .c-Yi7')
            cur_key = ele_a_datetime.get_attribute('href')
            dict_post['key'] = cur_key
            print(cur_key)

            ele_datetime = browser.find_one('._1o9PC', ele_a_datetime)
            date_time = ele_datetime.get_attribute('datetime')
            dict_post['datetime'] = date_time

            if hashtag == 1 and rnum > 9:
                compare_date = True

            if compare_date and datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%S.%fZ') < dateuntil:
                pbar.update(num)
                break

            # Fetching all img
            content = None
            img_urls = set()
            while True:
                ele_imgs = browser.find('._97aPb img', waittime=10)
                for ele_img in ele_imgs:
                    if content is None:
                        content = ele_img.get_attribute('alt')
                    img_urls.add(ele_img.get_attribute('src'))

                next_photo_btn = browser.find_one('._6CZji .coreSpriteRightChevron')
                if next_photo_btn:
                    next_photo_btn.click()
                    sleep(0.2)
                else:
                    break

            dict_post['content'] = content
            dict_post['img_urls'] = list(img_urls)

            # Fetching author post
            ele_comment = browser.find_one('.eo2As .gElp9')
            if ele_comment:
                author = browser.find_one('.FPmhX', ele_comment).text
                comment = browser.find_one('span', ele_comment).text
            dict_post['content'] = comment
            dict_post['author'] = author
            dict_post['comment_num'] = comment_num
            dict_post['like_num'] = like_num
            dict_post['view_num'] = view_num

            # check if video
            like_btn = browser.find_one('.eo2As .EDfFK.ygqzn .HbPOm._9Ytll .vcOH2')
            if like_btn:
                view_num = browser.find_one('span', like_btn).text
                print(view_num)
                browser.js_click(like_btn)
                like_info = browser.find_one('.eo2As .EDfFK.ygqzn .HbPOm._9Ytll .vJRqr span')
                if like_info:
                    like_num = int(like_info.text.replace(',',''))
                    
                print(like_num)
                
                dict_post['like_num'] = like_num
                dict_post['view_num'] = view_num


            # ilhamfajar - Load All Comments
            # browser.set_window_size(500, 768)
            load_all_comment = True

            i = 0
            while load_all_comment:                
                load_more_btn = browser.find_one('.Z4IfV')
                if load_more_btn:
                    browser.element_into_view(load_more_btn)
                    #print("Got Load More")
                    if load_more_btn.is_enabled():
                        load_more_btn.click()
                        sleep(0.5)
                        # harus ada metode cek apakah browser sudah selesai proses load more comment atau belum
                    else:
                        for iwait in range(1, 10):
                            #print("Wait for enabling load more")
                            sleep(1)
                            if load_more_btn.is_enabled():
                                load_more_btn.click()
                                break                            
                else:
                    #print("Can't Find Load More")
                    break

            # Fetching comments
            ele_comments = browser.find('.eo2As .gElp9')[1:]
            comments = []
            comment_num = len(ele_comments)
            for els_comment in ele_comments:
                author = browser.find_one('.FPmhX', els_comment).text
                comment = browser.find_one('span', els_comment).text
                comments.append({
                    'author': author,
                    'comment': comment,
                })
                
            if comments:
                dict_post['comments'] = comments
            if load_all_comment:
                dict_post['comment_num'] = comment_num

            # experimental - Fetching Like
            user_likes = []
            like_number = 0
            crawl_like = False

            like_btn = browser.find_one('.eo2As .Nm9Fw ._0mzm-.sqdOP.yWX7d._8A5w5')
            if like_btn:
                print(like_btn.text)
                if like_btn.text == "like this":
                    print("Found post with 0 like")
                    like_number = 0
                like_number = like_btn.text
                like_number = like_number.replace('like this','0')
                like_number = like_number.replace('likes','')
                like_number = like_number.replace('like','')
                like_number = int(like_number.replace(',',''))
                #print(like_number)
                if like_number > 0:
                    like_btn.click()
                time.sleep(1)

                #show_like = 0
                get_like = 0
                if crawl_like:
                    while get_like < like_number:
                        whois_like = browser.find('.wo9IH .uu6c_ .t2ksc .d7ByH .FPmhX')[get_like:]
                        #show_like = len(whois_like)
                        #print(show_like, " ", get_like)
                        for user in whois_like:
                            #print("name : ", user.text)
                            username = user.text
                            user_likes.append({
                                'username': username,
                            })
                            get_like = get_like + 1
                        print("Try to Scroll")
                        #driver.execute_script('arguments[0].scrollIntoView(true);', user)
                        browser.element_into_view(user)
                        #time.sleep(0.2)
                        #if get_like > 300:
                        #    break            

            if like_number > 0:
                dict_post['like_num'] = like_number   
            if user_likes:
                dict_post['user_likes'] = user_likes



            self.log(json.dumps(dict_post, ensure_ascii=False))
            dict_posts[browser.current_url] = dict_post

            pbar.update(1)
            left_arrow = browser.find_one('.HBoOv')
            if left_arrow:
                #left_arrow.click()
                browser.js_click(left_arrow)

        pbar.close()
        posts = list(dict_posts.values())
        posts.sort(key=lambda post: post['datetime'], reverse=True)
        return posts[:num]

    def _get_posts(self, num, login, dateuntil):
        '''
            To get posts, we have to click on the load more
            button and make the browser call post api.
        '''
        TIMEOUT = 600
        browser = self.browser
        key_set = set()
        posts = []
        pre_post_num = 0
        wait_time = 1

        pbar = tqdm(total=num)

        def start_fetching(pre_post_num, wait_time):
            ele_posts = browser.find('.v1Nh3 a')
            for ele in ele_posts:
                key = ele.get_attribute('href')
                if key not in key_set:
                    ele_img = browser.find_one('.KL4Bh img', ele)
                    content = ele_img.get_attribute('alt')
                    img_url = ele_img.get_attribute('src')
                    key_set.add(key)
                    posts.append({
                        'key': key,
                        'content': content,
                        'img_url': img_url
                    })
            if pre_post_num == len(posts):
                pbar.set_description('Wait for %s sec' % (wait_time))
                sleep(wait_time)
                pbar.set_description('fetching')

                wait_time *= 2
                browser.scroll_up(300)
            else:
                wait_time = 1

            pre_post_num = len(posts)
            browser.scroll_down()

            return pre_post_num, wait_time

        pbar.set_description('fetching')
        while len(posts) < num and wait_time < TIMEOUT:
            post_num, wait_time = start_fetching(pre_post_num, wait_time)
            pbar.update(post_num - pre_post_num)
            pre_post_num = post_num

            loading = browser.find_one('.W1Bne')
            if (not loading and wait_time > TIMEOUT/2):
                break

        pbar.close()
        print('Done. Fetched %s posts.' % (min(len(posts), num)))
        return posts[:num]

    def _get_specific_post_full(self, ele_post):
        # Get specific post from profile.
        # parameter :
        # - ele_post : specific post from profile page. element that contain ('.v1Nh3 a')
        dict_post = {}
        
        browser = self.browser
        browser.implicitly_wait(1)

        #pbar = tqdm(7)
        #pbar.set_description('fetching')

        # Fetch Num of View (if Video), Num of Likes, Num of Comments from layer when mouse over post's picture 
        # before click
        comment_num = 0
        like_num = 0
        view_num = 0
        browser.element_into_view(ele_post)
        browser.hover_on_element(ele_post)
        ele_hovers = browser.find('.qn-0x .Ln-UN .-V_eO', waittime=10)
        for ele_hover in ele_hovers:
            ele_num = browser.find_one('span', ele_hover).text
            if browser.find_one('._1P1TY.coreSpriteSpeechBubbleSmall', ele_hover):
                comment_num = ele_num
            elif browser.find_one('._1P1TY.coreSpriteHeartSmall', ele_hover):
                like_num = ele_num
            elif browser.find_one('._1P1TY.coreSpritePlayIconSmall', ele_hover):
                view_num = ele_num

        print(comment_num, " ", like_num," ", view_num)
        #pbar.update(1)

        # element clicked, get into post
        ele_post.click()

        dict_posts = {}

        cur_key = None

        dict_post['author']= ''
        # Fetching datetime and url as key
        ele_a_datetime = browser.find_one('.eo2As .c-Yi7')
        cur_key = ele_a_datetime.get_attribute('href')
        dict_post['key'] = cur_key

        #pbar.set_description('fetching ', cur_key)

        ele_datetime = browser.find_one('._1o9PC', ele_a_datetime)
        date_time = ele_datetime.get_attribute('datetime')
        dict_post['datetime'] = date_time

        #pbar.update(2)
        
        # Fetching all img
        content = None
        img_urls = set()
        while True:
            ele_imgs = browser.find('._97aPb img', waittime=10)
            for ele_img in ele_imgs:
                
                if content is None:
                    content = ele_img.get_attribute('alt')
                img_src = ele_img.get_attribute('src')
                print("*", img_src)
                if img_src:
                    img_urls.add(ele_img.get_attribute('src'))

            next_photo_btn = browser.find_one('._6CZji .coreSpriteRightChevron')
            if next_photo_btn:
                next_photo_btn.click()
                sleep(0.2)
            else:
                break

        dict_post['content'] = content
        dict_post['img_urls'] = list(img_urls)

        #pbar.update(3)

        # Fetching author post
        ele_comment = browser.find_one('.eo2As .gElp9')
        if ele_comment:
            author = browser.find_one('.FPmhX', ele_comment).text
            comment = browser.find_one('span', ele_comment).text
        dict_post['content'] = comment
        dict_post['author'] = author
        dict_post['comment_num'] = comment_num
        dict_post['like_num'] = like_num
        dict_post['view_num'] = view_num

        #pbar.update(4)

        # check if video
        like_btn = browser.find_one('.eo2As .EDfFK.ygqzn .HbPOm._9Ytll .vcOH2')
        if like_btn:
            view_num = browser.find_one('span', like_btn).text
            print(view_num)
            browser.js_click(like_btn)
            like_info = browser.find_one('.eo2As .EDfFK.ygqzn .HbPOm._9Ytll .vJRqr span')
            if like_info:
                like_num = int(like_info.text.replace(',',''))
                
            print(like_num)
            
            dict_post['like_num'] = like_num
            dict_post['view_num'] = view_num

        #pbar.update(5)

        # ilhamfajar - Load All Comments
        #browser.set_window_size(500, 768)
        
        # while False : load only comment visible
        # while True : load all comment
        #while False:
        while True:
            load_more_btn = browser.find_one('.Z4IfV')
            if load_more_btn:
                browser.element_into_view(load_more_btn)
                #print("Got Load More")
                if load_more_btn.is_enabled():
                    load_more_btn.click()
                    sleep(0.2)
                else:
                    for iwait in range(1, 10):
                        #print("Wait for enabling load more")
                        sleep(1)
                        if load_more_btn.is_enabled():
                            load_more_btn.click()
                            break                            
            else:
                #print("Can't Find Load More")
                break

        # Fetching comments
        ele_comments = browser.find('.eo2As .gElp9')[1:]
        comments = []
        for els_comment in ele_comments:
            author = browser.find_one('.FPmhX', els_comment).text
            comment = browser.find_one('span', els_comment).text
            comments.append({
                'author': author,
                'comment': comment,
            })

        if comments:
            dict_post['comments'] = comments

        #pbar.update(6)

        # experimental - Fetching Like
        # only for post is image, skipped when post is video
        user_likes = []
        like_number = 0

        # crawl_like :
        # - True : get number of likes and get list of user that like this post
        # - False : skip user list, only get number of likes        
        crawl_like = False

        like_btn = browser.find_one('.eo2As .Nm9Fw ._0mzm-.sqdOP.yWX7d._8A5w5')
        if like_btn:
            like_number = browser.find_one('span', like_btn).text
            like_number = int(like_number.replace(',',''))
            #print(like_number)
            like_btn.click()
            time.sleep(1)

            get_like = 0
            if crawl_like:
                while get_like < like_number:
                    whois_like = browser.find('.wo9IH .uu6c_ .t2ksc .d7ByH .FPmhX')[get_like:]
                    for user in whois_like:
                        #print("name : ", user.text)
                        username = user.text
                        user_likes.append({
                            'username': username,
                        })
                        get_like = get_like + 1
                    print("Try to Scroll")
                    browser.element_into_view(user)

        if like_number > 0:
            dict_post['like_num'] = like_number   
        if user_likes:
            dict_post['user_likes'] = user_likes

        #pbar.update(7)


        self.log(json.dumps(dict_post, ensure_ascii=False))

        # Click X on top right, close post
        close_btn = browser.find_one('._2dDPU.vCf6V .ckWGn')
        if close_btn:
            close_btn.click()

        ## Click X on top right, close post
        #close_btn = browser.find_one('._2dDPU.vCf6V .ckWGn')
        #if close_btn:
        #    #left_arrow.click()
        #    browser.js_click(close_btn)

        #pbar.close()
        print("I'm Done")

        return dict_post
   
    def get_user_posts_detail(self, username, number=None, login=False):

        browser = self.browser
        post_num = 0

        if login:
            self.login()
            # Check "Turn On Notification"
            sleep(0.2)
            not_now_btn = browser.find_one(".piCib .mt3GC .aOOlW.HoLwm")
            if not_now_btn:
                not_now_btn.click()

        user_profile = self.get_user_profile(username)
        if not number:
            number = instagram_int(user_profile['post_num'])

        self._dismiss_login_prompt()

        browser.implicitly_wait(1)
        ele_header = browser.find_one('.XjzKX ._2dbep')
        ele_posts = browser.find('.v1Nh3 a')

        dict_posts = {}
        
        # Fetching all posts
        while post_num < number:

            # Fetching all posts that displayed
            for ele_post in ele_posts:
                
                ## Test Procedure
                #comment_num = 0
                #like_num = 0
                #view_num = 0
                #browser.hover_on_element(ele_post)
                cur_key = ele_post.get_attribute('href')
                print(cur_key)
                #ele_hovers = browser.find('.qn-0x .Ln-UN .-V_eO', waittime=5)
                #if ele_hovers:               
                #    for ele_hover in ele_hovers:
                #        ele_num = browser.find_one('span', ele_hover, waittime=2).text
                #        if browser.find_one('._1P1TY.coreSpriteSpeechBubbleSmall', ele_hover):
                #            comment_num = ele_num
                #        elif browser.find_one('._1P1TY.coreSpriteHeartSmall', ele_hover):
                #            like_num = ele_num
                #        elif browser.find_one('._1P1TY.coreSpritePlayIconSmall', ele_hover):
                #            view_num = ele_num
                #    print(comment_num, " ", like_num," ", view_num)
                #ele_post.click()
                #sleep(5)

                # Real Procedure
                dict_post = {}
                cur_key = ele_post.get_attribute('href')
                print(cur_key)
                #dict_post = self._get_specific_post_full(ele_post)


                
                dict_posts[cur_key] = dict_post

                browser.hover_on_element(ele_header)
                browser.implicitly_wait(1)

                post_num = post_num + 1

                if post_num == number:
                    break

                
                #browser.element_into_view(ele_header)
                
            # focus on last post
            browser.element_into_view(ele_post)                        
            # wait
            browser.implicitly_wait(1)
            # check if there's new post
            ele_posts = browser.find('.v1Nh3 a')[post_num:]

        #posts = list(dict_posts.values())
        #posts.sort(key=lambda post: post['datetime'], reverse=True)

        #return posts[:post_num]
        return post_num

    def get_posts_by_tag(self, tag, number=None, detail=False, login=False, dateuntil=19000101):
        browser = self.browser
        
        if login:
            self.login()
            # Check "Turn On Notification"
            sleep(0.2)
            not_now_btn = browser.find_one(".piCib .mt3GC .aOOlW.HoLwm")
            if not_now_btn:
                not_now_btn.click()

        url = '%s/explore/tags/%s/' % (InsCrawler.URL, tag)
        browser.get(url)
        #return self._get_posts(num)

        #user_profile = self.get_user_profile(username)
        #if not number:
        #    number = instagram_int(user_profile['post_num'])
        if not number:
            number=100

        self._dismiss_login_prompt()

        if detail:
            return self._get_posts_full(number, login, datetime.strptime(dateuntil, '%Y%m%d'), 1)
        else:
            return self._get_posts(number, login, datetime.strptime(dateuntil, '%Y%m%d'))